from django import forms
from .models import Agenda

class FormAgenda(forms.Form):
    attrs = {
        'class': 'form-control'
        }
    activity = forms.CharField(label="Activity")
    day = forms.CharField(label="Day")
    date = forms.DateField(widget=forms.DateInput(attrs={'type' : 'date', 'class': 'form-control'}))
    time = forms.TimeField(widget=forms.TimeInput(attrs={'type' : 'time', 'class': 'form-control'}))
    place = forms.CharField(label="Place")
    category = forms.CharField(label="Category")
