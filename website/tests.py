from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.apps import apps
from .apps import WebsiteConfig
from .models import Agenda
from .views import portofolio
from django.http import HttpRequest

# Create your tests here.
class StoryUnitTest(TestCase):
    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, portofolio)

    def test_landing_page_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_landing_page_contains_hello(self):
        request = HttpRequest()
        response = portofolio(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<title>Portofolio</title>', html_response)
