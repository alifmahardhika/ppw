from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.portofolio, name='portofolio'),
    path('agenda', views.agenda, name='agenda'),
    path('create-agenda/', views.create_agenda, name='create_agenda'),
    path('clear_agenda', views.clear_agenda, name='clear_agenda'),
    
    
]

